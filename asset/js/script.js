$(document).ready(function(){
        $('.phone-menu-btn').click(function(){
            $('.phone-menu').slideToggle(500)
        })
        var items = [
            {name: "ជំនាញមេកានិក", img:"slide5.jpg"},
            {name: "អគ្គិសនី", img: "slide4.jpg"},
            {name: "អេឡិចត្រូនិច", img: "slide4.jpg"}
        ]
        getSlide();
        function getSlide(){
            var txt ="";
            items.forEach((e)=>{
                txt +=`
                <div class="slide">
                    <img src="asset/img/img-slide/${e['img']}" alt="">
                    <div class="content">
                        <h2>${e['name']}</h1>
                        <hr style="border: 2px dashed white">
                        <h1>WELCOME TO <br>REGINAL POLYTECHNIC INSTITUTE TECHO SEN SVAY RIENG</h1>
                        <hr style="border: 2px dashed white">
                        <h3>TVET ជំនាញពិតជីវិតប្រសើរ</h3>
                        <button type="button">ចុះឈ្មោះចូលរៀន</button>
                    </div>
                </div>
                `
            })
            console.log(txt)
            $(".slide-container").find('.box').append(txt);
        }
        var slide = $('.slide');
        var numSlide = slide.length;
        console.log(slide)
        var indSlide = 0;
        slide.hide();
        slide.eq(0).show();
        function nextSlide(){
            slide.eq(indSlide).hide();
            indSlide++;
            if(indSlide == numSlide){
                indSlide =0;
            }
            slide.eq(indSlide).show();
        }
        var mySlide = setInterval(function(){
            nextSlide();
        },6000);
        $('.slide-container').mouseover(function(){
            clearInterval(mySlide);
        })
        $('.slide-container').mouseleave(function(){
            mySlide = setInterval(function(){
                nextSlide();
            },6000);
        })
        $('.btnNext').click(function(){
            nextSlide();        
        })
        $('.btnBack').click(function(){
            slide.eq(indSlide).hide();
            indSlide--;
            if(indSlide < 0){
                indSlide = numSlide - 1;
            }
            slide.eq(indSlide).show()
        })
        var items_logo = [
            {name:"DGTVET",img:"DGTVET.jpg"},
            {name:"npic",img:"npic.png"},
            {name:"ntti",img:"NTTI.png"},
            {name:"nib",img:"NIB.jpg"},
            {name:"jvc",img:"JVC.jpg"},
            {name:"rtc",img:"rtc.png"},
            {name:"7",img:"7_.png"},
            {name:"8",img:"8_.png"},
            {name:"9",img:"9_.png"},
            {name:"10",img:"10_.png"},
            {name:"11",img:"11_.png"},
            {name:"12",img:"12_.png"},
            {name:"13",img:"13_.png"},
            {name:"14",img:"14.jpg"},
            {name:"15",img:"15.jpg"}
        ]
        function get_logo(){
            var txt='';
            items_logo.forEach((e)=>{
                txt +=`
                    <div class="box-mou-logo">
                        <img src="asset/img/mou/${e['img']}" alt="">
                    </div>
                `;

            })
            console.log(txt)
            $('.mou-log').append(txt);
        }
        get_logo();
})
